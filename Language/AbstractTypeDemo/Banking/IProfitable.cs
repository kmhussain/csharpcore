namespace Banking
{
    public interface IProfitable
    {
        decimal GetInterest(int period);
    }
}
