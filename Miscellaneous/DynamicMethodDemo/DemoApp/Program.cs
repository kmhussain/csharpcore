﻿using System;

namespace DemoApp
{
    class Program
    {
        static void Main(string[] args)
        {
            new Support().Run(args);
        }
    }
}
