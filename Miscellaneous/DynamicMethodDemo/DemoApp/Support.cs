using System;
using System.Reflection.Emit;

namespace DemoApp
{
    class Operators<T>
    {
        public static readonly Func<T, T, T> Add = MakeAddOperation();

        private static Func<T, T, T> MakeAddOperation()
        {
            Type t = typeof(T);
            Type[] signature = { t, t };
            DynamicMethod dm = new DynamicMethod("Add", t, signature, typeof(Operators<T>));
            ILGenerator gen = dm.GetILGenerator();

            gen.Emit(OpCodes.Ldarg_0);
            gen.Emit(OpCodes.Ldarg_1);
            if (t.IsPrimitive)
                gen.Emit(OpCodes.Add);
            else
                gen.EmitCall(OpCodes.Call, t.GetMethod("op_Addition", signature), null);
            gen.Emit(OpCodes.Ret);

            return (Func<T, T, T>)dm.CreateDelegate(typeof(Func<T, T, T>));
        }

    }

    class Support
    {
        private static V Sum<V>(V first, params V[] values)
        {
            V total = first;

            foreach (V value in values)
                total = Operators<V>.Add(total, value);

            return total;
        }

        public void Run(string[] args)
        {
            double sd = Sum(1.1, 2.3, 3.5, 4.7);
            Console.WriteLine("Sum of doubles = {0}", sd);

            Interval si = Sum(new Interval(2, 20), new Interval(3, 30), new Interval(4, 40));
            Console.WriteLine("Sum of Intervals = {0}", si);
        }
    }
}
