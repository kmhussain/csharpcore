<h2>C# Programming with .NET Core</h2>
<ul>
  <li>
  <b>Language Fundamentals</b>
  <ol>
    <li><i><a href="Language/MethodDemo/DemoApp">Methods</a></i></li>
    <li><i><a href="Language/StructTypeDemo/DemoApp">Struct Types</a></i></li>
    <li><i><a href="Language/ClassTypeDemo/DemoApp">Class Types</a></i></li>
    <li><i><a href="Language/GenTypeDemo/DemoApp">Generic Types</a></i></li>
    <li><i><a href="Language/PropertyDemo/DemoApp">Properties</a></i></li>
    <li><i><a href="Language/OperatorDemo/DemoApp">Operator Overloading</a></i></li>
    <li><i><a href="Language/BaseClassDemo1/DemoApp">Class Inheritance</a></i></li>
    <li><i><a href="Language/BaseClassDemo2/DemoApp">Object Class</a></i></li>
    <li><i><a href="Language/AbstractTypeDemo">Abstract Types</a></i></li>
    <li><i><a href="Language/GenInterfaceDemo/DemoApp">Generic Interfaces</a></i></li>
  </ol>
  </li>
  <li>
  <b>Runtime Fundamentals</b>
  <ol>
    <li><i><a href="Runtime/DelegateDemo1/DemoApp">Delegates</a></i></li>
    <li><i><a href="Runtime/DelegateDemo2/DemoApp">Events</a></i></li>
    <li><i><a href="Runtime/AsyncDemo/DemoApp">Asynchrony</a></i></li>
    <li><i><a href="Runtime/LinqDemo1/DemoApp">LINQ Basics</a></i></li>
    <li><i><a href="Runtime/LinqDemo2/DemoApp">Advanced LINQ</a></i></li>
    <li><i><a href="Runtime/ExpressionsDemo/DemoApp">Expression Trees</a></i></li>
    <li><i><a href="Runtime/AttributeDemo/DemoApp">Custom Attributes</a></i></li>
    <li><i><a href="Runtime/DynamismDemo/DemoApp">Dynamic Typing</a></i></li>
    <li><i><a href="Runtime/InteropDemo1">P/Invoke Basics</a></i></li>
    <li><i><a href="Runtime/InteropDemo2">Advanced P/Invoke</a></i></li>
  </ol>
  </li>
  <li>
  <b>Platform Fundamentals</b>
  <ol>
    <li><i><a href="Platform/ThreadingDemo/DemoApp">Threads</a></i></li>
    <li><i><a href="Platform/MonitorDemo/DemoApp">Monitor</a></i></li>
    <li><i><a href="Platform/FileIODemo/DemoApp">File I/O</a></i></li>
    <li><i><a href="Platform/DataIODemo/DemoApp">Data I/O</a></i></li>
    <li><i><a href="Platform/SerializationDemo/DemoApp">Object Serialization</a></i></li>
    <li><i><a href="Platform/XmlSerializerDemo/DemoApp">XML Serialization</a></i></li>
    <li><i><a href="Platform/NamedPipeDemo/DemoApp">Named Pipes</a></i></li>
    <li><i><a href="Platform/TcpSocketDemo/DemoApp">TCP Sockets</a></i></li>
    <li><i><a href="Platform/UdpSocketDemo/DemoApp">UDP Sockets</a></i></li>
    <li><i><a href="Platform/HttpClientDemo/DemoApp">HTTP Communication</a></i></li>
  </ol>
  </li>
  <li>
  <b>ASP.NET Fundamentals</b>
  <ol>
    <li><i><a href="AspNet/WebHostDemo/DemoApp">Web Host</a></i></li>
    <li><i><a href="AspNet/MvcAppDemo1/DemoApp">MVC Basics</a></i></li>
    <li><i><a href="AspNet/MvcAppDemo2/DemoApp">Advanced MVC</a></i></li>
    <li><i><a href="AspNet/RazorPageDemo/DemoApp">Razor Pages</a></i></li>
    <li><i><a href="AspNet/EFModelDemo1/DemoApp">Entity Framework Basics</a></i></li>
    <li><i><a href="AspNet/EFModelDemo2/DemoApp">Advanced Entity Framework</a></i></li>
    <li><i><a href="AspNet/AuthenticationDemo/DemoApp">Authentication</a></i></li>
    <li><i><a href="AspNet/WebApiDemo/DemoApp">Web API</a></i></li>
    <li><i><a href="AspNet/WebSocketDemo/DemoApp">Web Sockets</a></i></li>
    <li><i><a href="AspNet/SignalRDemo/DemoApp">Signal-R</a></i></li>
  </ol>
  </li>
</ul>
<p>
  All code samples have been tested on Windows 10, macOS 10.14 and RaspberryPi 3 with <a href="https://dotnet.microsoft.com/download/dotnet-core/2.2">.NET Core SDK 2.2</a>
</p>
<p>
  <i>&copy;2019 K.M Hussain. All rights reserved.</i>
</p>
